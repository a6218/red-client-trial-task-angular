# Probleme beim Lösen der Aufgabe

### Abkürzungen
- KP -> keine Probleme
- P  -> Probleme
- R  -> repariert

### 1. Fehlersuche [40 Minuten]
1. README, Fhir lesen und Patient, Practitioner APIs anschauen [KP]
2. Übersicht über das Projekt verschaffen [P]
    - Service, Variablen und Definitionen, die unbekannt sind
3. Fehlersuche [R]
    - im Dashboard wird ein falscher Abstrakter-Service importiert [R]

### 2. Erstelle eine Suchform [4 Stunden]
1. Bei der Suchform sollten Reactive Forms verwendet werden [KP]
    - entsprechendes Modul gesucht und implementiert
2. Erstelle ein Formular mit einem Input für eine Freitextsuche und einem Dropdown für einen Filter [KP]
    - input- und select-tag angelegt
3. Validierung des Suchfelds **(drei Möglichkeiten um es zu lösen)**
	  1. Zeichen müssen mit 'trim', 'regex' oder 'indexOf' im Such-String überprüft werden [KP]
		  - Vorteile:
			  - Paste wird auch sofort mit überprüft
		  - Nachteile:
			  - Such-String muss immer auf Zeichen überprüft
	  2. Such-String wird nur beim drücken der Enter-Taste überprüft [KP]
    3. Eingabe wird sofort blockieren [KP]
		  - Vorteile:
			  - Benutzer muss Zeichen nicht extra wieder löschen
		  - Nachteile:
			  - Paste Überprüfung muss extra implementiert werden
4. Suche-String + DropDown Select an Parent übergeben [KP]
5. Tabelle mit Filterregeln neu laden [P]
    - nur geringe Kenntnisse zu Observable und was diese machen!
    - Variablen mit $ nie zuvor gesehen, bis jetzt noch nicht ganz verstanden!
    - **!!! müsste hier fragen wie man es besser machen könnte !!!**
    
### 3. Detailsansicht [4 Stunden]
1. suche nach passender HTML-Komponente, um Klick-Event zu setzen [KP]
2. einbau des Details-Dialogs [KP]
3. Daten an die Dialog-Details übergeben [P]
    - benutze UtilsService direkt und die Daten vorzubereiten
    - erstelle weiteres Objekt mit allen Feldern
    - sende dieses Objekt an die Dialog-Details-Komponente
    - **!!! müsste hier fragen wie man es besser machen könnte !!!**
4. Darstellung der Daten in Dialog-Details [P]
    - Datenübergabe nicht sauber
    - HTML-Struktur nicht sauber
    - **!!! müsste hier fragen wie man es besser machen könnte !!!**

### 4. Erstellen von Unittests mit Jest für die SearchFacadeService [0]
1. noch nicht genauer angeschaut [P]
