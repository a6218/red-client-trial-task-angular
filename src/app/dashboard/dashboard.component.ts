import { Component } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, map, shareReplay, startWith, switchMap, tap } from 'rxjs/operators';
import { SiteTitleService } from '@red-probeaufgabe/core';
import { FhirSearchFn, IFhirPatient, IFhirPractitioner, IFhirSearchResponse } from '@red-probeaufgabe/types';
import { IUnicornTableColumn } from '@red-probeaufgabe/ui';

/** 
 * wrong service import
 * abstract classes should be uses for extends and not direct imports
*/
// import { AbstractSearchFacadeService } from '@red-probeaufgabe/search';
import { SearchFacadeService } from '@red-probeaufgabe/search';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {
  // Init unicorn columns to display
  columns: Set<IUnicornTableColumn> = new Set<IUnicornTableColumn>([
    'number',
    'resourceType',
    'name',
    'gender',
    'birthDate',
  ]);
  isLoading = true;

  searchFilter: FhirSearchFn = FhirSearchFn.SearchAll;
  searchKey: string = '';

  /*
   * Implement search on keyword or fhirSearchFn change
   **/
  search$: Observable<IFhirSearchResponse<IFhirPatient | IFhirPractitioner>> | undefined;

  entries$: Observable<Array<IFhirPatient | IFhirPractitioner>> | undefined;

  totalLength$: Observable<number> | undefined;

  ngOnInit() {
    this.refreshTableData();
  }

  constructor(private siteTitleService: SiteTitleService, private searchFacade: SearchFacadeService) {
    this.siteTitleService.setSiteTitle('Dashboard');
  }

  private handleError(): Observable<IFhirSearchResponse<IFhirPatient | IFhirPractitioner>> {
    return of({ entry: [], total: 0 });
  }

  refreshTableData(): void {
    this.isLoading = true;
    this.search$ = this.searchFacade
      .search(this.searchFilter, this.searchKey)
      .pipe(
        catchError(this.handleError),
        tap((data) => {
          this.isLoading = false;
        }),
        shareReplay(),
      );

    this.entries$ = this.search$.pipe(
      map((data) => !!data && data.entry),
      startWith([]),
    );

    this.totalLength$ = this.search$.pipe(
      map((data) => !!data && data.total),
      startWith(0),
    );
  }

  changeSearchFilter(searchValue: string): void {
    this.searchKey = searchValue;
    this.refreshTableData();
  }

  changeSelectFilter(selectValue: string): void {
    this.searchFilter = FhirSearchFn.SearchAll;

    if (selectValue === 'patient') {
      this.searchFilter = FhirSearchFn.SearchPatients;
    } else if (selectValue === 'practitioner') {
      this.searchFilter = FhirSearchFn.SearchPractitioners;
    }

    this.refreshTableData();
  }
}
