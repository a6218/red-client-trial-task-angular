import { Component, Inject, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UnicornTableComponent } from '../unicorn-table/unicorn-table.component';

export interface DialogData {
  id: string,
  recourceType: string,
  birthDate: string,
  gender: string,
  name: string,
  address: string,
}

@Component({
  selector: 'app-dialog-detail-row',
  templateUrl: './dialog-detail-row.component.html',
  styleUrls: ['./dialog-detail-row.component.scss'],
})
export class DialogDetailRowComponent {
  @Input() label = '';
  @Input() data = '';

  constructor(
    public dialogRef: MatDialogRef<UnicornTableComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: DialogData
  ) {}

  onCloseDialog(): void {
    this.dialogRef.close();
  }

  displayTelecom(row): boolean {
    if (row !== undefined && 0 in row && row[0] !== undefined) {
      return true;
    }

    return false;
  }
}
