import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { IUnicornTableColumn } from '../models';
import { IFhirPatient, IFhirPractitioner } from '@red-probeaufgabe/types';
import { MatDialog } from '@angular/material/dialog';
import { FhirUtilService } from '@red-probeaufgabe/search';
import { DialogDetailRowComponent } from '../dialog-detail-row/dialog-detail-row.component';
import { check } from 'prettier';

@Component({
  selector: 'app-unicorn-table',
  templateUrl: './unicorn-table.component.html',
  styleUrls: ['./unicorn-table.component.scss'],
})
export class UnicornTableComponent implements OnInit {
  dataSource: MatTableDataSource<IFhirPatient | IFhirPractitioner> = new MatTableDataSource([]);
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @Input() columns: Set<IUnicornTableColumn> = new Set<IUnicornTableColumn>();
  @Input() totalLength = 0;
  @Input() isLoading = false;

  @Input()
  set entries(value: Array<IFhirPatient | IFhirPractitioner>) {
    this.dataSource.data = value;
  }

constructor(
  public dialog: MatDialog,
  private fhirUtilsService: FhirUtilService
) {}

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }

  onRowClick(row): void {
    let preparedData = this.fhirUtilsService.prepareData(row);

    let dialogData = {
      id: preparedData.id ?? 'unknown',
      recourceType: preparedData.resourceType ?? 'unknown',
      birthDate: preparedData.birthDate ?? 'unknown',
      gender: preparedData.gender ?? 'unknown',
      name: 0 in preparedData.name && preparedData.name[0] ? preparedData.name[0] : 'unknown',
      address: preparedData.address ?? 'unknown',
      telecom: preparedData.telecom ?? undefined,
    };

    const dialogRef = this.dialog.open(
      DialogDetailRowComponent,
      { data: dialogData }
    );
  }
}
