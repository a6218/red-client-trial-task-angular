import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss'],
})
export class SearchFormComponent {
  /** Implement Search Form */

  @Output() searchInputEvent = new EventEmitter<string>();
  @Output() selectInputEvent = new EventEmitter<string>();

  showInputError: boolean = false;
  errorMessage: string = '';

  searchForm = this.formBuilder.group({
    search: [''],
    select: [''],
  });

  constructor(private formBuilder: FormBuilder) {}

  onlySpaces(searchValue: string): boolean {
    if (searchValue.trim().length === 0) {
      return true;
    }

    return false;
  }

  validInput(searchValue: string): boolean {
    if (this.onlySpaces(searchValue)) {
      return true;
    }

    if (searchValue.indexOf('ä') !== -1  
      || searchValue.indexOf('ö') !== -1
      || searchValue.indexOf('ü') !== -1
      || searchValue.indexOf(' ') !== -1
    ) {
      this.showInputError = true;
      this.errorMessage = "Bitte geben Sie keine Leerzeichen, 'ä', 'ü' oder 'ö' ein!";
      return false;
    }

    return true;
  }

  /* first solution */
  onSearch(searchValue: string): void {
    this.showInputError = false;
    if (!this.validInput(searchValue)) {
      return;
    }

    this.searchInputEvent.emit(searchValue);
  }

  onSelect(selectValue: string): void {
    this.selectInputEvent.emit(selectValue);
  }
  /* first solution */

  /* second solution */
  onSubmit(): void {
    let searchValue = this.searchForm.value.search;
    this.showInputError = false;

    if (!this.validInput(searchValue)) {
      return;
    }

    this.searchInputEvent.emit(searchValue);
  }
  /* second solution */

  /* third solution */
  /* Problem: extra paste check has to be made */
  blockSpace(event): void {
    this.showInputError = true;
    this.errorMessage = 'Bitte geben Sie keine Leerzeichen ein!';
    event.preventDefault();
  }

  blockCharacters(event): void {
    let keyPressed = event.key;
    if (keyPressed === 'ä' 
      || keyPressed === 'ü' 
      || keyPressed === 'ö' 
    ) {
      this.showInputError = true;
      this.errorMessage = 'Bitten geben Sie kein "ä", "ü" oder "ö" ein!';
      event.preventDefault();
    }
  }
  /* third solution */

}
